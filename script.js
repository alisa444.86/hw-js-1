/*
* let & const - ключевые слова, с помощью которых объявляются переменные.
*
* const - постоянная величина, значение которой нельзя менять (переназначать).
* Исключение - свойства объекта: в этом случае навзание переменной не меняется,
* но можно изменить пары ключ-значение внутри объекта.
* const нельзя объявить, не присвов ей значение.
*
* let - переменная, значение которой можно менять. Видна только внутри блока, в котором создана. Это позволяет
* использовать переменные с одинаковым названием в разных блоках кода. В отличие от const, let можно объявить
* до присвоения ей значения.
*
* */

let name = prompt("Enter your name", '');
let age = prompt("How old are you?", '');

if (!name) {
    name = prompt("Enter your name", `${name}`);
}

if (isNaN(Number(age))) {
    age = prompt("How old are you?", `${age}`);
}

if (Number(age) < 18) {
    alert("You are not allowed to visit this website");
} else if (Number(age) > 18 && Number(age) <= 22) {
    let confirmation = confirm("Are you sure you want to continue?");
    if (confirmation === true) {
        alert(`Welcome, ${name}!`);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert(`Welcome, ${name}!`);
}
